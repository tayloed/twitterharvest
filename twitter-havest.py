import twitter       #Twitter api https://pypi.python.org/pypi/python-twitter/3.3 (pip install python-twitter)
import datetime
from datetime import timedelta
import re            #regular expression engine
import argparse      #https://pypi.python.org/pypi/argparse/1.4.0

################################################################################################
# Twitter-harvest.py is a simple program that searches twitter looking for keywords found
# in the input file (--input). It will then check the text of each tweet to see if there are 
# words or phrases that should be ignored. The ignore words are found in the --Ignore file.
#
# The output is places into an html formatted document. 
#
# Created by Tayloe Draughon www.draughon.us
# 2017.06.13
################################################################################################
#
# Upgrades needed:
# 1. In the text that is output (r.text) from the twitter api there is often an embedded URL.
#    This embedded URL should be printed out onto a separate line.
#         https://www.draughon.us
# 2. Once the URL is printed onto a second line, then it should be preceded and followed with
#    the html tags so that a user can click on the link and then go directly to the text.
#    The html tags will be '<a href="https://www.draughon.us">' URL text then '</a>'


################################################################################################
# twitter-harvest.py main processing loop.
#
def main():
    
    article_dict = {} # store tweets by keywords
    then = datetime.datetime.now() - timedelta(hours = 1)
    
    #TweetDate = datetime.datetime.strptime('Fri Aug 18 17:50:07', '%a %b %H:%M:%S')
    
    print ("{}\tINFO\tStarting twitter-havest program.".format(datetime.datetime.now()))
 
    ############################################
    # parsing arguments to get input parameters
   
    parser = argparse.ArgumentParser(description='twitter-havest" to scan twitter for desired terms.')
    parser.add_argument("-i", "--input", help="The name of the input file fully qualified",  required=True)
    parser.add_argument("-o", "--output", help="The name of the fully qualified output file",  required=True)
    parser.add_argument("-n", "--NumberOfResults", help="The maximum number of results in each search", default=20, required=False)
    parser.add_argument("-x", "--Ignore", help="The name of the fully qualified file of strings to ignore",  default="ignore.txt", required=False)
    
    # parse arguments
    args = vars(parser.parse_args())
    print("args=")
    print(args)
    for key, value in args.items():
        print("key = %s value =%s"%(key,value))
    ######################################################################################
    # connect to the twitter api so we can search for terms found in the input file name
    #api = twitter.Api(consumer_key="GetFromTwitter https://apps.twitter.com/",
    #                  consumer_secret="GetFromTwitter https://apps.twitter.com/",
    #                  access_token_key="GetFromTwitter https://apps.twitter.com/",
    #                  access_token_secret="GetFromTwitter https://apps.twitter.com/")
    
    api = twitter.Api(consumer_key="imjeGD1nKzqgaB4Zl8nNXdtyC",
                      consumer_secret="5RrGcn9xITcOctJwsVYG4kc4tgoE9rQzukI2GIiMztfUPpxCzW",
                      access_token_key="1603742406-BQQDBJCYSBsJIgDn5NUhSoIqrBkfDl5DKaD4gW7",
                      access_token_secret="WxEoe8Ig5gCd6YoGThdFfCgSganZukHwsLA9yqdCJhkrr")
    
    print ("{}\tINFO\ttwitter-havest connected to twitter api.".format(datetime.datetime.now()))
    
    maxCount = 25
        
    outputFile = open(args['output'], "w")
    outputFile.write("<!DOCTYPE html><HTML><HEAD><H1>Twitter Harvest for {}</H1>\n".format(args['input']))
    outputFile.write("</HEAD>\n")
    outputFile.write("<P>{}</P>".format(datetime.datetime.now()))
    # CSS settings 
    outputFile.write("<style>\n p { \n text-wrap: normal; \n width:initial; \n color: darkgreen; }")
    outputFile.write("div.toc {\n    width:800px;\n    max-width:800px;\n    margin: auto;\n    border: 1px solid #73AD21;}\n")
    outputFile.write("</style>\n")
   
	
    print("{}\tINFO\ttwitter-havest program writing output file.".format(datetime.datetime.now()))
    
    #read in list of regular expressions to ignore.
    xList = list()
    xfh = open(args['Ignore'])
    for xline in xfh.readlines():
        xList.append(xline.strip())
        
    
    # read through each line of the input file and search twitter for the term found on each line.
    searchList = list()
    fh = open(args['input'])
    
    outputFile.write("<h2>Contents</h2><ol>")
    for line in fh.readlines():
        line = line.rstrip()
        outputFile.write('<li><a href="#{}">{}</a></li>'.format(line, line))
        searchList.append(line)
    outputFile.write("</ol>")   
    
    
    # prepare the article dictionary so that each of the key words will have 
    # a section in the output file regardless of the source site 
    for key in searchList:
        article_dict[key] = []
    
    
    
    for sTerm in searchList:
        print ("{}\tINFO\tSearch on '{}'.".format(datetime.datetime.now(), sTerm))
        SearchByHashTag(article_dict, api, sTerm, maxCount, outputFile,xList)
        
     #TODO
     # iterate through article_dict and print out the titles
     # do not repeat tweets 
     # move output to here
    
    for keyword in searchList:
        outputFile.write('<a name="{0}"></a><h2>{1}</h2>\n'.format(keyword, keyword))
        #OnlyOnceList = [] # reset Only Once List at the change in each keyword.
                          # We want to output each link once in each keyword section
                          # but it is OK to output the link in another section.
        if len(article_dict[keyword]) == 0:
             outputFile.write('<br> <i>-- no tweets found for {} --</i>'.format(keyword))
                             
        for item in article_dict[keyword]:
            try:
                outputFile.write("<P>")
                if item[0] != None and item[1] != None:
                    outputFile.write('<b><a href="https://twitter.com/{}">@{}</a></b>'.format( item[0], item[0])) #Twitter handle
                    if item[1] != None:
                        outputFile.write('<br>{0}\n'.format(item[1]))  # tweet
                    if item[2] != None:
                        outputFile.write('<br>{0}\n'.format(item[2]))  # publish date
                        #Fri Aug 18 17:50:07
                        #TweetDate = datetime.strptime(item[2], '%a %b %H:%M:%S')
                        #if(TweetDate > then):
                        #    outputFile.write('<br>FRESH')
                        #else:
                        #    outputFile.write('<br>stale')
                            
                        
                        
                    if item[3] != None and item[4] != None:
                        outputFile.write('<br>Favorite:{0} Retweet:{1}'.format(item[3],item[4]))  # fav and retweet counts
                    if item[6] != None:
                        tUrl = item[6].url
                        if tUrl == None:
                            tUrl = item[6].expanded_url 
                        if tUrl != None:
                            outputFile.write('<br><a href="{0}">{1}</a>\n'.format(tUrl,tUrl)) # article link
                            
                    if item[5] != None:
                        outputFile.write('{0}'.format(item[5]))  # Hastags
            except:
                pass
            
            outputFile.write("</P>\n")
            #OnlyOnceList.append(item[2])
        
    #article_dict[hashtag].append((r.user.screen_name, r.text, r.created_at, r.favorite_count, r.retweet_count, sHashTags ))
    
        
    
    outputFile.write("<P><br><br>completed at {}</P>".format(datetime.datetime.now()))   
    outputFile.write("</HTML>\n")
    outputFile.close()
    
    print("{}\tINFO\ttwitter-harvest program completed.".format(datetime.datetime.now()))
    print("{}\tINFO\tDone.".format(datetime.datetime.now()))


################################################################
# The SearchByHashTag searches for any string in "hashtag"
# on twitter. It then compares the text items found in xlist
# and discards anything that matches the xlist.
#################################################################
def SearchByHashTag(article_dict, api, hashtag, maxCount, outputFile, xlist):
    #print ("Searching {}".format(hashtag))
    results = api.GetSearch(hashtag,  None, None, None, None, None, None, maxCount)
    for r in results:
        if(r.user.lang == "en"):
            try:
                
                #if r.urls != None:
                bKeep = True
                for x in xlist:
                    myMatch = re.search(x, r.text, re.I)
                    if myMatch == None:
                        pass
                    else:
                        bKeep = False
                        #print ("skipping:{} | found in:{}".format(x, r.text))
               
                if  bKeep == True:
                    nnn = len(r.urls)
                    if len(r.urls) != 0:
                        sHashTags = "<pre>\t"
                        for h in r.hashtags:
                            #build a string of all hashtags
                            sHashTags += " #{} ".format( h.text )
                        
                        sHashTags += "</pre>"
                        article_dict[hashtag].append((r.user.screen_name, r.text, r.created_at, r.favorite_count, r.retweet_count, sHashTags, r.urls[0] ))
                                   
            except:
                pass
                


#######################################
if __name__ == '__main__': 
    main()



