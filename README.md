# TwitterHarvest Overview

Twitter-harvest.py is a simple program that searches twitter looking for keywords found
in the input file (--input). It will then check the text of each tweet to see if there are 
words or phrases that should be ignored. The ignore words are found in the --Ignore file.

The output is places into an html formatted document. 

# Basic overview
##Dependencies 
*Python-Twitter API. https://pypi.python.org/pypi/python-twitter
**(pip install python-twitter)
*ArgParse https://pypi.python.org/pypi/argparse/1.4.0
**(pip install argparse)

##How to run
* python twitter-havest.py -i Input.txt -o Ouptut.html -x ignore.txt



# Who do I talk to? 
Repo Owner
* tayloed
* www.draughon.us